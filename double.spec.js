const double = require('./double')

test('double of 1 is 2', () => {
  expect(double(1)).toBe(2)
})

test('double of 3 is 6', () => {
  expect(double(3)).toBe(6)
})

test('double gets error with string input', () => {
  try {
    double('a')
  } catch (ex) {

  }
})

// TODO: add one more test case
test('double of 6 is 12', () => {
  expect(double(6)).toBe(12)
})
